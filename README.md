# Full Vuexy theme dockerized

The purpose is dockerized the full Vuexy theme for development and production. The project is based on the 
[vuexy theme](https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/documentation/development/getting-started/overview.html)
All project use docker and docker compose to develop and run. 

## Project development setup

For the development setup, you need to have the compose.build.yml file in the root of the project. This file is used to 
build the project and install all the dependencies. The configuration use volume to map the project folder to the 
container. This way, you can edit the code and the changes will be reflected in the container. the container use the tty 
to allow you to use the terminal inside the container. 

1. Run the container to start the development environment
```bash
docker compose -f compose.build.yaml up -d
```

2. Enter the container
```bash
docker compose -f compose.build.yaml exec node bash
```
3. Install the dependencies
```bash
npm install
```

4. Run the development server
```bash
npm run dev -- --host 0.0.0.0 --port 8095
```

This run the server for the dev mode and allow you to access the server from the host machine. You need to specify the 
host and the port because is a docker installation and the access is over network. 

5. Open the browser and go to http://localhost:8095

## Project run setup

This is a run setup similar to production but use only the node image to run the project. This is useful to test the 
environment in local reducing the resources used by the container and the build time. Tu run this setup you need to the 
web server is http-server. This is a simple web server that serve the static files.

1. Run the container to start the development environment
```bash
docker compose -f compose.dev.yaml up --build -d
```

2. Open the browser and go to http://localhost:8095

## Project production setup

This is the production setup. This setup use the nginx image to serve the static files. The configuration file is copied 
to the image in the build time. This setup is useful to test the production environment in local.

1. Run the container to start the development environment
```bash
docker compose -f compose.yaml up --build -d
```

2. Open the browser and go to http://localhost:8095
