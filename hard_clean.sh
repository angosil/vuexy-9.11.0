echo "1) Stop all running containers"
docker stop $(docker ps -a -q)

echo "2) Delete all containers"
docker rm $(docker ps -a -q)

echo "3) Delete all images"
docker rmi $(docker images -q)

echo "4) Delete all volumes"
docker volume rm $(docker volume ls -q)

echo "5) Delete all networks"
docker network rm $(docker network ls -q)

echo "6) Prune build cache"
docker builder prune -a -f

echo "7) prepare tu use the build kit image"
docker buildx use default
