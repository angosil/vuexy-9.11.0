# syntax=docker/dockerfile:1

ARG NODE_VERSION=20.11.0

################################################################################
# Use node image for base image for all stages.
FROM node:${NODE_VERSION}-bookworm-slim as base

# Set working directory for all build stages.
WORKDIR /app

################################################################################
# Create a stage for installing dependencies.
FROM base as install

# syntax=docker/dockerfile:1

ARG NODE_VERSION=20.11.0

################################################################################
# Use node image for base image for all stages.
FROM node:${NODE_VERSION}-bookworm-slim as base

# Set working directory for all build stages.
WORKDIR /app

################################################################################
# Create a stage for installing dependencies.
FROM base as install

# Copy package.json and package-lock.json into the image.
COPY package*.json ./

# Copy the specific file into the image.
COPY ../../src/plugins/iconify/build-icons.ts ./src/plugins/iconify/

# Install dependencies.
RUN npm ci

################################################################################
# Create a stage for building the application.
FROM install as build

# Copy the rest of the source files into the image.
COPY . .

# Build the application.
RUN npm run build --production

################################################################################
# Create a final stage for serving the application.
FROM registry.access.redhat.com/ubi8/nginx-122 as final

# Copy the Nginx configuration file into the image.
COPY build/prod/nginx.conf "${NGINX_CONF_PATH}"

# Copy the built application from the build stage.
COPY --from=build /app/dist /opt/app-root/src

# Redirect Nginx logs to stdout and stderr.
RUN ln -sf /dev/stdout /var/log/nginx/access.log \
    && ln -sf /dev/stderr /var/log/nginx/error.log

# Set the permissions of the application files.
USER 0
RUN chown -R 1001:0 /opt/app-root/src && chmod -R ug+rwx /opt/app-root/src
USER 1001

# Expose the port that the application listens on.
EXPOSE 8080

# Serve the application.
CMD ["nginx", "-g", "daemon off;"]
